---
title: "Project 0"
date: 2019-05-16T10:56:50-05:00
draft: false
---

**Sign up for [Jobmachine Gitlab Group](https://gitlab.com/jobmachine).**

**Submit ideas to the curricula project.**

Update this post as needed.

General things to discuss:
* What styleguide for what languages (PEP 8)?
* How do we structure this documentation (provision for documentation, links, blog posts)?
* What workflows are relevant?
* What supplemental services should we consider (Trello etc)

Specific Project Ideas:
* Slack bot
  * Submit coding challenges throughout the week on a particular day it opens for voting.
  * Notify people in the slack when there are commits or outstanding pull requests.

Interest Areas (scon).
* Collaborative Development Workflow
* Python
* SRE/Devops/Automation
* Data Analysis
* Monitoring
* Documentation, Tech Writing and Presentation Skills
* Interviewee Skills
* AI/ML
* Natural Language Processing
