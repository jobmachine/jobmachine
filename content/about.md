+++
title = "About"
date = "2019-05-16"
aliases = ["about-us","about-hugo","contact"]
author = "Fiona Buckner"
+++

This website is dedicated to Jobmachine's dedication to continuous improvement.  We will be utilizing this site to keep up with projects we are currently working on in order to improve our programming skills.
